$(function(){
    $("#bahan-plus").click(function(){
        var count = Number($("#bahan-count").val());
        count++;
        $("#bahan").append(
            '<div class="row">'+
                '<div class="col-md-1">'+
                (count) +
                '</div>'+
                '<div class="col-md-11">'+
                    '<input type="text" class="form-control" name="bahan-'+count+'">'+
                '</div>'+
            '</div>'
        );
        $("#bahan-count").val(count);
    })
})
