<?php

namespace backend\controllers;

use Yii;
use backend\models\Mountain;
use backend\models\MountainSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\assets\AppAsset;

/**
 * MountainController implements the CRUD actions for Mountain model.
 */
class MountainController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mountain models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MountainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mountain model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mountain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mountain();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            
            $file = UploadedFile::getInstance($model, 'mountain_image');
            if(!empty($file)){
                // mengambil file upload
                $date = date('d-m-y_h-i-s');
                $imageName = $date;
                $file->saveAs('../web/image/mountain/'.$imageName.'.'.$file->extension);

                // save path on db
                $model->mountain_image = '/image/mountain/'. $imageName .'.'. $file->extension;
            }
            if($model->save()){
            return $this->redirect(['view', 'id' => $model->mountain_id]);
        }
        return "error";
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Mountain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mountain_image = $model->mountain_image;
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            
            $file = UploadedFile::getInstance($model,'mountain_image');
            if(!empty($file)){
                // mengambil file upload
                $date = date('d-m-y_h-i-s');
                $imageName = $date;
                $file->saveAs('../web/image/mountain/'.$imageName.'.'.$file->extension);

                // save path on db
                $model->mountain_image = '/image/mountain/'. $imageName .'.'. $file->extension;
            }else{
                $model->mountain_image = $mountain_gambar;
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->mountain_id]);
            }
            return "error";
        } else {
                return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Mountain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mountain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mountain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mountain::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
