<?php

namespace backend\controllers;

use Yii;
use backend\models\Friend;
use backend\models\FriendSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FriendController implements the CRUD actions for Friend model.
 */
class FriendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Friend models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FriendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Friend model.
     * @param integer $friend_id
     * @param integer $id_friend
     * @return mixed
     */
    public function actionView($friend_id, $id_friend)
    {
        return $this->render('view', [
            'model' => $this->findModel($friend_id, $id_friend),
        ]);
    }

    /**
     * Creates a new Friend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Friend();

        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $model->users_id = Yii::$app->user->getId();
            if($model->save()){
            return $this->redirect(['view', 'friend_id' => $model->friend_id, 'id_friend' => $model->id_friend]);
            }
            /* @var $attribute type */
            return "Data tidak ditemukan";
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Friend model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $friend_id
     * @param integer $id_friend
     * @return mixed
     */
    public function actionUpdate($friend_id, $id_friend)
    {
        $model = $this->findModel($friend_id, $id_friend);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'friend_id' => $model->friend_id, 'id_friend' => $model->id_friend]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Friend model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $friend_id
     * @param integer $id_friend
     * @return mixed
     */
    public function actionDelete($friend_id, $id_friend)
    {
        $this->findModel($friend_id, $id_friend)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Friend model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $friend_id
     * @param integer $id_friend
     * @return Friend the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($friend_id, $id_friend)
    {
        if (($model = Friend::findOne(['friend_id' => $friend_id, 'id_friend' => $id_friend])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
