<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Mountain */

$this->title = $model->mountain_id;
$this->params['breadcrumbs'][] = ['label' => 'Mountains', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mountain-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mountain_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mountain_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mountain_id',
            'mountain_name',
            'mountain_address',
            'mountain_height',
            [
                'attribute' =>'mountain_image',
                'value'     => $model->mountain_image,
                'format'    => ['image',['width' => 100,'heigth' => 100]],
            ],
            'mountain_description:ntext',
            'mountain_created_at',
            'mountain_updated_at',
        ],
    ]) ?>

</div>
