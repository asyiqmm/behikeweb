<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Mountain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mountain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mountain_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mountain_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mountain_height')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mountain_image')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mountain_description')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
