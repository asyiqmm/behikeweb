<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MountainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mountains';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mountain-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Mountain', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'mountain_id',
            'mountain_name',
//            'mountain_address',
//            'mountain_height',
            'mountain_image',
            // 'mountain_description:ntext',
            'mountain_created_at',
            'mountain_updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
