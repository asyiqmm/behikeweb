<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ClimbSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="climb-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'climb_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'climb_mountain_id') ?>

    <?= $form->field($model, 'climb_created_at') ?>

    <?= $form->field($model, 'climb_updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
