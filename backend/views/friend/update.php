<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Friend */

$this->title = 'Update Friend: ' . $model->friend_id;
$this->params['breadcrumbs'][] = ['label' => 'Friends', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->friend_id, 'url' => ['view', 'friend_id' => $model->friend_id, 'id_friend' => $model->id_friend]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="friend-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
