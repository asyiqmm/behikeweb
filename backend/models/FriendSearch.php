<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Friend;

/**
 * FriendSearch represents the model behind the search form about `backend\models\Friend`.
 */
class FriendSearch extends Friend
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['friend_id', 'users_id', 'id_friend'], 'integer'],
            [['friend_created_at', 'friend_updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Friend::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'friend_id' => $this->friend_id,
            'users_id' => $this->users_id,
            'friend_created_at' => $this->friend_created_at,
            'friend_updated_at' => $this->friend_updated_at,
            'id_friend' => $this->id_friend,
        ]);

        return $dataProvider;
    }
}
