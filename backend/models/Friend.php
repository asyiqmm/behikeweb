<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "friend".
 *
 * @property integer $friend_id
 * @property integer $users_id
 * @property string $friend_created_at
 * @property string $friend_updated_at
 * @property integer $id_friend
 *
 * @property Users $users
 * @property Users $idFriend
 */
class Friend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'id_friend'], 'required'],
            [['users_id', 'id_friend'], 'integer'],
            [['friend_created_at', 'friend_updated_at'], 'safe'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['id_friend'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_friend' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'friend_id' => 'Friend ID',
            'users_id' => 'Users ID',
            'friend_created_at' => 'Friend Created At',
            'friend_updated_at' => 'Friend Updated At',
            'id_friend' => 'Id Friend',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasOne(Users::className(), ['id' => 'users_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFriend()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_friend']);
    }
}
