-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 15, 2017 at 07:21 AM
-- Server version: 10.2.11-MariaDB-10.2.11+maria~xenial-log
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `behike`
--

-- --------------------------------------------------------

--
-- Table structure for table `climb`
--

CREATE TABLE `climb` (
  `climb_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `climb_mountain_id` int(11) NOT NULL,
  `climb_created_at` timestamp NULL DEFAULT current_timestamp(),
  `climb_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `climb`
--

INSERT INTO `climb` (`climb_id`, `user_id`, `climb_mountain_id`, `climb_created_at`, `climb_updated_at`) VALUES
(1, 6, 4, '2017-12-12 17:21:37', '2017-12-12 17:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL,
  `device_uuid` varchar(100) NOT NULL,
  `device_name` varchar(100) NOT NULL,
  `users_id` int(11) NOT NULL,
  `device_created_at` timestamp NULL DEFAULT current_timestamp(),
  `device_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`device_id`, `device_uuid`, `device_name`, `users_id`, `device_created_at`, `device_updated_at`) VALUES
(1, 'uuiidiunadiw', 'beacon_coba', 4, '2017-12-04 15:33:24', '2017-12-04 15:33:24'),
(2, '1231', 'bea', 6, '2017-12-12 17:07:55', '2017-12-12 17:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE `friend` (
  `friend_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `friend_created_at` timestamp NULL DEFAULT current_timestamp(),
  `friend_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_friend` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend`
--

INSERT INTO `friend` (`friend_id`, `users_id`, `friend_created_at`, `friend_updated_at`, `id_friend`) VALUES
(8, 4, '2017-12-04 15:29:37', '2017-12-04 15:29:37', 6),
(9, 6, '2017-12-12 17:08:57', '2017-12-12 17:08:57', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mountain`
--

CREATE TABLE `mountain` (
  `mountain_id` int(11) NOT NULL,
  `mountain_name` varchar(45) DEFAULT NULL,
  `mountain_address` varchar(255) DEFAULT NULL,
  `mountain_height` varchar(25) DEFAULT NULL,
  `mountain_image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `mountain_description` longtext DEFAULT NULL,
  `mountain_created_at` timestamp NULL DEFAULT current_timestamp(),
  `mountain_updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mountain`
--

INSERT INTO `mountain` (`mountain_id`, `mountain_name`, `mountain_address`, `mountain_height`, `mountain_image`, `mountain_description`, `mountain_created_at`, `mountain_updated_at`) VALUES
(1, 'Gunung Bromo', 'Terletak di Kabupaten Probolinggo, Pasuruan, Lumajang, dan Kabupaten Malang', '2.392 MDPL', '/image/mountain/04-12-17_03-56-15.jpg', 'Gunung Bromo terkenal sebagai objek wisata utama di Jawa Timur. Sebagai sebuah objek wisata, Bromo menjadi menarik karena statusnya sebagai gunung berapi yang masih aktif. Gunung Bromo termasuk dalam kawasan Taman Nasional Bromo Tengger Semeru. Bentuk tubuh Gunung Bromo bertautan antara lembah dan ngarai dengan kaldera atau lautan pasir seluas sekitar 10 kilometer persegi. Gunung Bromo mempunyai sebuah kawah dengan garis tengah ± 800 meter (utara-selatan) dan ± 600 meter (timur-barat). Sedangkan daerah bahayanya berupa lingkaran dengan jari-jari 4 km dari pusat kawah Bromo.', '2017-11-29 08:01:06', '2017-12-04 02:56:15'),
(4, 'Gunung Ijen', 'Terletak di perbatasan antara Kabupaten Banyuwangi dan Kabupaten Bondowoso', '2.443 MDPL', '/image/mountain/04-12-17_03-56-59.jpg', 'Untuk mencapai kawah Gunung Ijen dari Banyuwangi, bisa menggunakan kereta api ekonomi dengan tujuan Banyuwangi dan turun di Stasiun Karangasem kemudian naik ojek dengan tujuan Kecamatan Licin dan Desa Banyusari. Dari Banyusari, perjalanan dilanjutkan menuju Paltuding dengan menumpang truk pengangkut belerang atau menggunakan bus dan turun di Banyuwangi kota kemudian naik ojek bisa langsung ke Paltuding atau ke Desa Banyusari juga bisa namun dengan menggunakan bus tarif yang dikeluarkan akan lebih mahal. Pintu gerbang utama ke Cagar Alam Taman Wisata Kawah Ijen terletak di Paltuding, yang juga merupakan Pos PHPA (Perlindungan Hutan dan Pelestarian Alam). ', '2017-11-29 08:36:59', '2017-12-04 02:56:59'),
(5, 'Gunung Kelud', 'Terletak di perbatasan Kabupaten Kediri, Kabupaten Blitar, dan Kabupaten Malang', '1.731 MDPL', '/image/mountain/04-12-17_03-57-11.jpg', 'Sebagaimana Gunung Merapi, Gunung Kelud merupakan salah satu gunung berapi paling aktif di Indonesia. Kekhasan gunung api ini adalah adanya danau kawah, yang dalam kondisi letusan dapat menghasilkan aliran lahar letusan dalam jumlah besar, dan membahayakan penduduk sekitarnya. Letusan freatik tahun 2007 memunculkan kubah lava yang semakin membesar dan menyumbat permukaan danau, sehingga danau kawah nyaris sirna, menyisakan genangan kecil seperti kubangan air. Kubah lava ini kemudian hancur pada letusan besar di awal tahun 2014.', '2017-11-29 08:42:12', '2017-12-04 02:57:11'),
(6, 'Gunung Arjuno', 'Terletak di perbatasan antara Kabupaten Malang dan Mojokerto', '3.339 MDPL', '/image/mountain/04-12-17_03-57-26.jpg', 'Gunung Arjuno merupakan gunung tertinggi ketiga di Jawa Timur setelah Gunung Semeru dan Gunung Raung, serta menjadi yang tertinggi keempat di Pulau Jawa. Biasanya gunung ini dicapai dari tiga titik pendakian yang cukup dikenal yaitu dari Lawang, Tretes dan Batu. Puncak Gunung Arjuno terletak pada satu punggungan yang sama dengan puncak gunung Welirang, sehingga kompleks ini sering disebut juga dengan Arjuno-Welirang. ', '2017-11-29 08:49:03', '2017-12-04 02:57:26'),
(7, 'Gunung Welirang', 'Terletak di perbatasan antara Kota Batu, Kabupaten Malang dan Mojokerto', '3.156 MDPL', '/image/mountain/04-12-17_03-57-38.jpg', 'Jalur pendakian dapat dilakukan melalui Desa Jubel, Kecamatan Pacet, Mojokerto. Di bagian sekitar puncak hidup tumbuhan endemik yang dinamakan penduduk setempat sebagai manis rejo.', '2017-11-29 08:53:05', '2017-12-04 02:57:38'),
(8, 'Gunung Penanggungan', 'Terletak di perbatasan Kabupaten Mojokerto dan Kabupaten Pasuruan', '1.653 MDPL', '/image/mountain/04-12-17_04-07-00.jpg', 'Gunung Penanggungan (dahulu bernama Gunung Pawitra) (1.653 m dpl) adalah gunung berapi kerucut (istirahat) yang terletak di Jawa Timur, Indonesia. Posisinya berada di dua kabupaten, yaitu Kabupaten Mojokerto (sisi barat) dan Kabupaten Pasuruan (sisi timur), berjarak kurang lebih 55 km dari Surabaya. ', '2017-11-29 08:57:22', '2017-12-04 03:07:00'),
(9, 'Gunung Raung', 'Terletak di antara Banyuwangi, Bondowoso, dan Jember', '3.344 MDPL', '/image/mountain/04-12-17_04-04-18.jpg', 'Gunung Raung merupakan gunung tertinggi kedua di Jawa Timur setelah Gunung Semeru, serta menjadi yang tertinggi keempat di Pulau Jawa. Kaldera Gunung Raung juga merupakan kaldera kering yang terbesar di Pulau Jawa dan terbesar kedua di Indonesia setelah Gunung Tambora di Nusa Tenggara Barat[4]. Terdapat empat titik puncak, yaitu Puncak Bendera, Puncak 17, Puncak Tusuk Gigi, dan, yang tertinggi, Puncak Sejati (3.344 ', '2017-11-29 09:00:12', '2017-12-04 03:04:18'),
(10, 'Gunung Argopuro', 'Terletak di Probolinggo', '3.088 MDPL', '/image/mountain/04-12-17_04-04-32.jpg', 'Gunung Argapura (sering dieja Gunung Argopuro) adalah sebuah gunung berapi kompleks yang terdapat di Jawa Timur, Indonesia. Gunung Argapura mempunyai ketinggian setinggi 3.088 meter. Gunung Argapura merupakan bekas gunung berapi yang kini sudah tidak aktif lagi.\r\n\r\nGunung ini berada kawasan Suaka Margasatwa Pegunungan Yang, sehingga kompleks ini sering disebut Yang-Argapura. Kompleks Yang-Argapura merupakan kompleks gunung berapi raksasa yang mendominasi bentang alam antara Gunung Raung dan Gunung Lemongan di Jawa Timur, Indonesia. Di kompleks ini terdapat untaian lembah sedalam 1.000 m. Pendakian Gunung Argapura memang terbilang memberikan tantangan tersendiri, pasalnya meskipun hanya memiliki ketinggian sekitar 3.088 mdpl, namun jalur pendakiannya cukup panjang. Tak heran jika disebut trek pendakian terpanjang di Pulau Jawa yaitu sekitar 63 Km.', '2017-11-29 09:04:32', '2017-12-04 03:04:32'),
(11, 'Gunung Lawu', 'Terletak di Kabupaten Karanganyar, Kabupaten Ngawi, dan Kabupaten Magetan', '3.265 MDPL', '/image/mountain/04-12-17_04-04-44.jpg', 'Status gunung ini adalah gunung api \"istirahat\" (diperkirakan terahkir meletus pada tanggal 28 November 1885[3][4]) dan telah lama tidak aktif, terlihat dari rapatnya vegetasi serta puncaknya yang tererosi. Di lerengnya terdapat kepundan kecil yang masih mengeluarkan uap air (fumarol) dan belerang (solfatara). Gunung Lawu memiliki tiga puncak, Puncak Hargo Dalem, Hargo Dumiling dan Hargo Dumilah. Yang terakhir ini adalah puncak tertinggi. Di lereng gunung ini terdapat sejumlah tempat yang populer sebagai tujuan wisata, terutama di daerah Tawangmangu, Cemorosewu, dan Sarangan. ', '2017-11-29 09:08:21', '2017-12-04 03:04:44'),
(12, 'Gunung Semeru', 'Terletak di Kabupaten Malang dan Kabupaten Lumajang', '3.676 MDPL', '/image/mountain/04-12-17_04-05-01.jpg', 'Semeru mempunyai kawasan hutan Dipterokarp Bukit, hutan Dipterokarp Atas, hutan Montane, dan Hutan Ericaceous atau hutan gunung. Posisi geografis Semeru terletak antara 8°06\' LS dan 112°55\' BT. Pada tahun 1913 dan 1946 Kawah Jonggring Saloka memiliki kubah dengan ketinggian 3.744,8 m hingga akhir November 1973.', '2017-11-29 09:11:25', '2017-12-04 03:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `authKey` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `authKey`, `username`, `created_at`, `updated_at`) VALUES
(4, 'asyiq', 'mubarok', 'asyiq@gmail.com', '$2y$10$2zUSLm1mjQKtG1uM82xySOBtzP6cQyUDKmhMTPyLUDpZqWYCi7LP2', '', 'asyiq', '2017-12-03 15:07:46', '2017-12-15 07:14:49'),
(5, 'dani', 'wadani', 'dani@g.com', '$2y$10$sMJX7Kfksln/iteaLiF/De0nGx9kf6bduegvh/t4NaHmMkSAN7eSW', '', 'dani', '2017-12-04 14:33:47', '2017-12-04 14:33:47'),
(6, 'asyiq', 'asd', 'a@a', '$2y$10$1l7mOIXN/pc1YqPETLOUZeo2ze6SHUnwiLpbcojXGCmNd8exnYuKq', '', 'admin', '2017-12-04 15:16:29', '2017-12-04 15:16:29'),
(9, 'Jefry', 'Dewangga', 'a@a', '$2y$10$uwL/mfFY1ry8EwbwqqoEdOzqtqzHUbHjuUqTcXRFwV974CAyBz25q', '', 'jefrydco', '2017-12-15 07:20:14', '2017-12-15 07:20:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `climb`
--
ALTER TABLE `climb`
  ADD PRIMARY KEY (`climb_id`),
  ADD KEY `fk_climb_table1` (`user_id`),
  ADD KEY `fk_climb_table2` (`climb_mountain_id`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`),
  ADD KEY `fk_device_users1` (`users_id`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`friend_id`,`id_friend`),
  ADD KEY `fk_friend_users1` (`users_id`),
  ADD KEY `fk_friend_users2` (`id_friend`);

--
-- Indexes for table `mountain`
--
ALTER TABLE `mountain`
  ADD PRIMARY KEY (`mountain_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `climb`
--
ALTER TABLE `climb`
  MODIFY `climb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `friend_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mountain`
--
ALTER TABLE `mountain`
  MODIFY `mountain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `climb`
--
ALTER TABLE `climb`
  ADD CONSTRAINT `fk_climb_table1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_climb_table2` FOREIGN KEY (`climb_mountain_id`) REFERENCES `mountain` (`mountain_id`);

--
-- Constraints for table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `fk_device_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `friend`
--
ALTER TABLE `friend`
  ADD CONSTRAINT `fk_friend_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_friend_users2` FOREIGN KEY (`id_friend`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
